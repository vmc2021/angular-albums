import {Component, OnInit, ViewChild} from '@angular/core';
import {Album} from "../../models/Album";
import {DataService} from "../../service/data.service";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
//========================================================================
export class AlbumsComponent implements OnInit {
  //properties go here
  albums: Album[] = []
  currentClasses: {} = {}; //empty object. used at the end
  currentStyle: {} = {};//empty object.
  enableAddAlbum: boolean = true

  otherAlbum: Album = {
    title: '',
    artist: '',
    songs: [],
    favorite: '',
    year: 0,
    genre: '',
    units: 0,
    isActive: true,
    cover: '',
    hide: false

  }
//@ViewChild decorator
  @ViewChild("albumForm") form: any;


  constructor(private dataService: DataService) {
  }

//========================================================================
  ngOnInit(): void {
//data goes here
//     this.albums = [
//       //5 objects
//       {
//         title: "Appetite for Destruction",
//         artist: "Guns N' Roses",
//         songs: ["Welcome to the Jungle, It's so Easy, Nighttrain, Out ta Get Me," +
//         "Mr.Brownstone, Paradise City, My Michelle, Think About You, Sweet Child O' Mine," +
//         "You're Crazy, Anything Goes, Rocket Queen"],
//         favorite: "Sweet Child O' Mine",
//         year: 1987,
//         genre: "Classic Rock",
//         units: 30000000,
//         cover: "../assets/img/appetitefordestruction.jpeg",
//         isActive: true,
//         hide: false
//
//       },
//       {
//         title: "White Pony",
//         artist: "Deftones",
//         songs: ["Feiticeira, Digital Bath, Elite, RX Queen, Street Carp, Teenager," +
//         "Knife Party, Korea, Passenger, Change(In the House of Flies), Pink Maggit, Elite(liverehearsal video"],
//         favorite: "Change (In The House of Flies",
//         year: 2000,
//         genre: "nu metal",
//         units: 20000000,
//         cover: "../assets/img/White_Pony.jpeg",
//         isActive: true,
//         hide: false
//
//       },
//       {
//         title: "American Idiot",
//         artist: "Green Day",
//         songs: ["American Idiot, Jesus of Suburbia, Holiday, Are We the Waiting, Give Me Novacaine," +
//         "Extraordinary Girl, Wake Me up When September Ends"],
//         favorite: "American Idiot",
//         year: 2004,
//         genre: "Punk rock",
//         units: 30000000,
//         cover: "../assets/img/GreenDay.jpg",
//         isActive: true,
//         hide: false
//
//       },
//       {
//         title: "Issues",
//         artist: "Korn",
//         songs: ["Dead, Falling Away from Me, Trash, 4u, Beg for Me, Make Me Bad, It's Gonna Go Away"],
//         favorite: "Falling Away from Me",
//         year: 1999,
//         genre: "Nu Metal",
//         units: 21000000,
//         cover: "../assets/img/korn.jpg",
//         isActive: true,
//         hide: false
//
//
//       },
//       {
//         title: "..And Justice for All",
//         artist: "Guns N' Roses",
//         songs: ["Blackened, ...And Justice for All, Eye of the Beholder, One, The Shortest Straw, Harvester of Sorrow"],
//         favorite: "One",
//         year: 1988,
//         genre: "Progressive Metal",
//         units: 30000000,
//         cover: "../assets/img/metallica.jpg",
//         isActive: true,
//         hide: false
//
//       }
//     ]//end of array

    this.setCurrentClasses();
    this.setCurrentStyle();// assign it to something in html like an element

    //access geAlbums() method
    this. dataService.getAlbums().subscribe(data => {
      this.albums = data;
    })


  }//end of ngOnInit

  //===== Methods go here ============================
  setCurrentClasses() {
    //calling and updating our property currentClasses
    this.currentClasses = {
      'btn-danger': this.enableAddAlbum //using class from bootstrap
      //add this to the OnInit
    }
  }

  //example of ngStyle
  setCurrentStyle() {
    //call and update the property currentStyle
    this.currentStyle = {
      "padding-top": "60px",
      "text-decoration": "underline"
    }
  }


  //methods go here. add a new album
  addAlbum() {
    //what we need to to do? add to beginning or end of array
    this.albums.unshift(this.otherAlbum); //unshift adds character to front of array

    //clear out the form /reset

    this.otherAlbum = {
      title: '',
      artist: '',
      songs: [],
      favorite: '',
      year: 0,
      genre: '',
      units: 0,
      isActive: true,
      cover: '../assets/img/nirvana.jpg',
      hide: false

    }
  }

//method to toggle the individual album's info
  toggleInfo(album: any) {
    //test it out
    // console.log("toggle info clicked");
    album.hide = !album.hide;
    //what is .hide? We need to create a property inside ngOnInit. add this property under memberSince.
    //make sure to add hide property in interface.
  }
  //new method
  onSubmit({value, valid} : {value: Album, valid:boolean}) {
    if (!valid) {
      alert("Form is not valid");

    }
    else{
      value.isActive = true;
      value.hide = false;
      console.log(value)
      //add the new data. update the else statement
      this.albums.unshift(value);
      //reset form /clear out the inputs
      this.form.reset();
    }

  }


}//end of export class
