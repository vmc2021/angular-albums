import { Injectable } from '@angular/core';
import {Album} from "../models/Album";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  //Albums interface
  albums: Album[] = [];


  constructor() {
    this.albums = [
      //5 objects
      {
        title: "Appetite for Destruction",
        artist: "Guns N' Roses",
        songs: ["Welcome to the Jungle, It's so Easy, Nighttrain, Out ta Get Me," +
        "Mr.Brownstone, Paradise City, My Michelle, Think About You, Sweet Child O' Mine," +
        "You're Crazy, Anything Goes, Rocket Queen"],
        favorite: "Sweet Child O' Mine",
        year: 1987,
        genre: "Classic Rock",
        units: 30000000,
        cover: "../assets/img/appetitefordestruction.jpeg",
        isActive: true,
        hide: false

      },
      {
        title: "White Pony",
        artist: "Deftones",
        songs: ["Feiticeira, Digital Bath, Elite, RX Queen, Street Carp, Teenager," +
        "Knife Party, Korea, Passenger, Change(In the House of Flies), Pink Maggit, Elite(liverehearsal video"],
        favorite: "Change (In The House of Flies",
        year: 2000,
        genre: "nu metal",
        units: 20000000,
        cover: "../assets/img/White_Pony.jpeg",
        isActive: true,
        hide: false

      },
      {
        title: "American Idiot",
        artist: "Green Day",
        songs: ["American Idiot, Jesus of Suburbia, Holiday, Are We the Waiting, Give Me Novacaine," +
        "Extraordinary Girl, Wake Me up When September Ends"],
        favorite: "American Idiot",
        year: 2004,
        genre: "Punk rock",
        units: 30000000,
        cover: "../assets/img/GreenDay.jpg",
        isActive: true,
        hide: false

      },
      {
        title: "Issues",
        artist: "Korn",
        songs: ["Dead, Falling Away from Me, Trash, 4u, Beg for Me, Make Me Bad, It's Gonna Go Away"],
        favorite: "Falling Away from Me",
        year: 1999,
        genre: "Nu Metal",
        units: 21000000,
        cover: "../assets/img/korn.jpg",
        isActive: true,
        hide: false


      },
      {
        title: "..And Justice for All",
        artist: "Guns N' Roses",
        songs: ["Blackened, ...And Justice for All, Eye of the Beholder, One, The Shortest Straw, Harvester of Sorrow"],
        favorite: "One",
        year: 1988,
        genre: "Progressive Metal",
        units: 30000000,
        cover: "../assets/img/metallica.jpg",
        isActive: true,
        hide: false

      }
    ]//end of array }


  }//end of constructor
  //methods
  getAlbums() : Observable<Album[]> {
    return of(this.albums);
  }

}//end of class
